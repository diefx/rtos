/*-------------------------------------------------------------------------------------------------
  @author  Hotboards Software Team
  @version V1.0.0
  @date    18-June-2014
  @brief   Wrapper definitions for FreeRTOS
-------------------------------------------------------------------------------------------------*/
#ifndef __rtos_h__
#define __rtos_h__

#ifdef __cplusplus
    extern "C" {
#endif


/* includes */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "timers.h"
#include "cli.h"

/* control os definitions */
#define _rtos_max_delay                       portMAX_DELAY
#define _rtos_tick_period_ms                  portTICK_PERIOD_MS
#define _rtos_scheduler_not_started           taskSCHEDULER_NOT_STARTED

/* control os macro definitions */
#define __rtos_disable_interrupts             taskDISABLE_INTERRUPTS
#define __rtos_enable_interrupts              taskENABLE_INTERRUPTS
#define __rtos_end_switching_isr              portEND_SWITCHING_ISR
#define __rtos_yield_from_isr                 portYIELD_FROM_ISR

/* typedef definitios */
#define rtos_cli_cmd_t                        CLI_Command_Definition_t
#define rtos_task_t                           TaskHandle_t
#define rtos_semphr_t                         SemaphoreHandle_t
#define rtos_queue_t                          QueueHandle_t
#define rtos_timer_t                          TimerHandle_t

/* Wrapper for memory management functions */
#define rtos_malloc                           pvPortMalloc
#define rtos_free                             vPortFree

/* Wrapper for cli functions */
#define rtos_cli_registerCommand              FreeRTOS_CLIRegisterCommand
#define rtos_cli_processCommand               FreeRTOS_CLIProcessCommand
#define rtos_cli_getOutputBuffer              FreeRTOS_CLIGetOutputBuffer
#define rtos_cli_getParameter                 FreeRTOS_CLIGetParameter

/* Wrapper for task functions */
#define rtos_task_create                      xTaskCreate
#define rtos_task_delay                       vTaskDelay
#define rtos_task_delayUntil                  vTaskDelayUntil

/* Redefinition for kernel functions */
#define rtos_kernel_startScheduler            vTaskStartScheduler
#define rtos_kernel_suspendAll                vTaskSuspendAll
#define rtos_kernel_resumeAll                 xTaskResumeAll
#define rtos_kernel_mallocFailedHook          vApplicationMallocFailedHook
#define rtos_kernel_getSchedulerState         xTaskGetSchedulerState
#define rtos_kernel_sysTickHandler            xPortSysTickHandler
#define rtos_kernel_idleTask                  vApplicationIdleHook
#define rtos_kernel_tickHook                  vApplicationTickHook
#define rtos_kernel_StackOverflowHook         vApplicationStackOverflowHook

#define rtos_queue_create                     xQueueCreate
#define rtos_queue_send                       xQueueSendToBack
#define rtos_queue_receive                    xQueueReceive
#define rtos_queue_sendFromIsr                xQueueSendToBackFromISR
#define rtos_queue_receiveFromIsr             xQueueReceiveFromISR

#define rtos_semphr_create                    xSemaphoreCreateBinary
#define rtos_semphr_take                      xSemaphoreTake
#define rtos_semphr_give                      xSemaphoreGive
#define rtos_semphr_takeFromIsr               xSemaphoreTakeFromISR
#define rtos_semphr_giveFromIsr               xSemaphoreGiveFromISR

#define rtos_timer_create                     xTimerCreate
#define rtos_timer_start                      xTimerStart


#ifdef __cplusplus
    }
#endif

#endif

